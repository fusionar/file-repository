# Fusionar File Repository #

Este repositorio fue creado con la intención de poder compartir archivos con personas que no pertenecen a la organización.

## ¿Cómo compartir un archivo?

Ir a la sección [Downloads](https://bitbucket.org/fusionar/file-repository/downloads/) y subir el archivo para compartirlo.

## Soporte

arodriguez@fusionar.com.uy